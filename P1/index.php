<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/validatePassword.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>

<title></title>
</head>
<body>
	<header>
		<h1><div align="center"><strong>Registration Form</strong></div></h1>
			<h4>If you already registered please <a href="#">sign in</a></h4>
	</header>
		<div id="reg" align="center" style="font-family: sans-serif; " name="form">
			<form ng-app="valApp" ng-controller="validateCtrl" method="post" name="myForm" novalidate>
				<label>e-Mail: <br/>
					<input type="email" name="emailn" id="uemail" placeholder="Enter your e-mail" ng-model="emailn" required><br></label>
					<span style="color:red" ng-show="myForm.emailn.$dirty && myForm.emailn.$invalid">
					<span ng-show="myForm.emailn.$error.required">e-Mail is required!</span>
					</span><br><br>
				<label>Username: <br/>
					<input type="text" name="unamen" id="uuname" placeholder="Enter your Username" ng-model="unamen" required><br></label>
					<span style="color:red" ng-show="myForm.unamen.$dirty && myForm.unamen.$invalid">
					<span ng-show="myForm.unamen.$error.required">Username is required!</span>
					</span><br><br>
				<label>Password: <br/>
					<input type="password" name="pwdn" id="upwd" placeholder="Enter your password" ng-model="pwdn" required><br></label>
					<span style="color:red" ng-show="myForm.pwdn.$dirty && myForm.pwdn.$invalid">
					<span ng-show="myForm.pwdn.$error.required">Without a password you cant log on!</span>
					</span><br><br>
				<label>Confirm Password: <br/>
					<input type="password" name="cpwdn" id="ucpwd" onkeyup="checkPass(); return false;" placeholder="Confirm your password!" ng-model="cpwdn" required><br></label>
					<span id="confirmMessage" class="confirmMessage"></span>
					<span style="color:red" ng-show="myForm.cpwdn.$dirty && myForm.cpwdn.$invalid">
					<span ng-show="myForm.cpwdn.$error.required">Please confirm it!</span>
					</span><br><br>
				<label>First Name: <br/>
					<input type="text" name="fnamen" id="ufname" placeholder="First part of your name!" ng-model="fnamen" required><br></label>
					<span style="color:red" ng-show="myForm.fnamen.$dirty && myForm.fnamen.$invalid">
					<span ng-show="myForm.fnamen.$error.required">We need to call you a name!</span>
					</span><br><br>
				<label>Last Name: <br/>
					<input type="text" name="lnamen" id="ulname" placeholder="Initials/Rest of your name"><br></label><br>
					<input type="submit" name="submitn" value="Sign me UP" size="5" id="usubmit">
			</form>
			<script>
var app = angular.module('valApp', []);
app.controller('validateCtrl', function($scope) {
    $scope.emailn = '';
    $scope.unamen = '';
    $scope.pwdn = '';
    $scope.cpwdn = '';
    $scope.fnamen = '';
});
</script>
		</div>
</body>
</html>
<!-- input {
    background-color:#FFFFFF;
    width:160px;
    
    /*
    * Demo only
    */
    position:absolute;
    top:50%;
    left:50%;
    transform:translate(-50%,-50%);
    -webkit-transform:translate(-50%,-50%);
}
input::-webkit-input-placeholder {
    color: #CCCCCC;
    position:relative;
    padding-left:0;
    -webkit-transition: padding 0.5s;
    /* For Safari 3.1 to 6.0 */
    transition: padding 0.5s;
}

/*input:-moz-placeholder {
    /* Firefox 18- */
    color: #CCCCCC;
    position:relative;
    padding-left:0;
    -moz-transition: padding 0.5s;
    /* For Safari 3.1 to 6.0 */
    transition: padding 0.5s;
}
input::-moz-placeholder {
    /* Firefox 19+ */
    color: #CCCCCC;
    position:relative;
    padding-left:0;
    -moz-transition: padding 0.5s;
    /* For Safari 3.1 to 6.0 */
    transition: padding 0.5s;
}
input:-ms-input-placeholder {
    color: #CCCCCC;
    position:relative;
    padding-left:0;
    -ms-transition: padding 0.5s;
    /* For Safari 3.1 to 6.0 */
    transition: padding 0.5s;
}
/*
* On hover
*/
 input:hover::-webkit-input-placeholder, input:focus::-webkit-input-placeholder {
    padding-left:100px;
}
input:hover:-moz-placeholder, input:focus:-moz-placeholder {
    /* Firefox 18- */
    padding-left:100px;
}
input:hover::-moz-placeholder, input:focus::-moz-placeholder {
    /* Firefox 19+ */
    padding-left:100px;
}
input:hover:-ms-input-placeholder, input:focus:-ms-input-placeholder {
    padding-left:100px;
} -->